﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delegate_Project
{
    internal class Program
    {
        
        
        static void Main(string[] args)
        {
            Student student = new Student(Guid.NewGuid().GetHashCode(),"AAA",DateTime.Now.AddMonths(-240),"Male");
            student.AllInfo();
            Console.ReadLine();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delegate_Project
{
    public class Student
    {
        private int StudentID { get; set; }
        private string Name { get; set; }
        private DateTime Birth { get; set; }
        private string Gender { get; set; }

        private delegate int Student_InfoID();

        private delegate string Student_Info_Note();
        private delegate DateTime Student_Info_Birth();

        public Student(int _studentID, string _name, DateTime _birth, string _gender)
        {
            StudentID = _studentID;
            Name = _name;
            Birth = _birth;
            Gender = _gender;
        }
        private int GetStudentID()
        {
            return StudentID;
        }

        private string GetName()
        {
            return Name;
        }

        private DateTime GetBirth()
        {
            return Birth;
        }

        private string GetGender()
        {
            return Gender;
        }

        public void AllInfo()
        {
            var studentID = new Student_InfoID(GetStudentID);
            var studentName = new Student_Info_Note(GetName);
            var studentGender = new Student_Info_Note(GetGender);
            var studentBirth = new Student_Info_Birth(GetBirth);

            Console.WriteLine($"\nID: {studentID()}\t Name: {studentName()}");
            Console.WriteLine($"Gender: {studentBirth()}\t Gender: {studentGender()}");

        }

    }
}

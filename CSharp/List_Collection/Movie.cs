﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace List_Collection
{
    public class Movie : IComparable<Movie>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Director { get; set; }

        public class PlayMovie
        {
            public void ShowMovie()
            {
                Console.WriteLine("Tại sao kêu tại class này rồi ko xài ?? =)))");
            }
        }

        public int CompareTo(Movie y)
        {
            if (Id > y.Id) return 1;
            else if (Id < y.Id) return -1;
            else return 0;
        }

        public static void ToList_desc(List<Movie> list)
        {
            foreach (Movie movie in list)
                Console.WriteLine($"Id: {movie.Id}\tName: {movie.Name}\tDirector: {movie.Director}");
        }

        public static Movie FindMovie_ByName (List<Movie> list, string Name)
        {
            foreach (var item in list)
            {
                if (item.Name == Name) return item; 
            }
            return null;
        }

        public static void FindResult(Movie movie4)
        {
            Console.WriteLine($"\n\n\nTim kiem Name =  {movie4.Name}");
            Console.WriteLine($"Id: {movie4.Id}\tName: {movie4.Name}\tDirector: {movie4.Director}");
        }
    }
}

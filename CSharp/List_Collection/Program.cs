﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace List_Collection
{
    internal class Program
    {
        

       
        static void Main(string[] args)
        {
            //a
            List<Movie> list = new List<Movie>()
            {
                new Movie{ Id = 11,Name = "Movie1",Director = "AAA" },
                new Movie{ Id = 223,Name = "Movie2",Director = "BBB" },
                new Movie{ Id = 311,Name = "Movie3",Director = "CCC" },
                new Movie{ Id = 49,Name = "Movie4",Director = "DDD" },

            };

            //b
            list.RemoveAt(0);
            //c
            list.Insert(2, new Movie { Id = 123, Name = "Movie5", Director = "EEE" });
            //d
            list.Sort();
            list.Reverse();

            Movie.ToList_desc(list);

            //e
            Movie movie4 = Movie.FindMovie_ByName(list,"Movie4");

            Movie.FindResult(movie4);
            Console.ReadLine();


        }
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IEnumerable_Ex
{
    internal class Program
    {
        

        
        static void Main(string[] args)
        {
            
            Module module1 = new Module(Guid.NewGuid(),"AAA",1,DateTime.Now,DateTime.Now.AddDays(90));
            Module module2 = new Module(Guid.NewGuid(), "BBB", 2, DateTime.Now.AddDays(-4), null);
            Module module3 = new Module(Guid.NewGuid(), "CCC", 3, DateTime.Now.AddDays(4), DateTime.Now.AddDays(94));

            Module[] lst = new Module[] { module1, module2, module3 };
            Modules modules = new Modules(lst);
            //2a
            

            foreach (Module module in modules)
            {
                Console.WriteLine($"\nId:{module.Id}\nName: {module.Name}\tEmployeeID:{module.EmployeeId}");
                Console.WriteLine($"Date:{module.Date}\t Completed Date: {module.CompletedDate}");
                Console.WriteLine($"Status: {module.Status}\n\n");
            }

            //2b

            Console.WriteLine("Dictionary\n\n\n");

            Dictionary<int, Module> di_list = new Dictionary<int, Module>();
            di_list.Add(module1.Id.GetHashCode(), module1);
            di_list.Add(module2.Id.GetHashCode(), module2);
            di_list.Add(module3.Id.GetHashCode(), module3);

            // Nên lấy property ID là key vì trước đó nó đã được tạo random ngay khi khởi tạo. 
            // Nếu hiện giờ sử dụng nó làm Key thì nó sẽ dc Random thêm 1 lần nữa.
            // ===> Tăng độ bảo mật

            foreach (Module module in di_list.Values)
            {
                Console.WriteLine($"\nId:{module.Id}\nName: {module.Name}\tEmployeeID:{module.EmployeeId}");
                Console.WriteLine($"Date:{module.Date}\t Completed Date: {module.CompletedDate}");
                Console.WriteLine($"Status: {module.Status}\n\n");
            }

            // câu 3 e xin GG
            Console.ReadLine();
        }
    }
}

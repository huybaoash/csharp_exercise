﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IEnumerable_Ex
{
    public class Modules : IEnumerable
    {
        private readonly Module[] _listModule;

        public Modules(Module[] listInt)
        {
            _listModule = listInt;
        }

        public IEnumerator GetEnumerator()
        {
            return _listModule.GetEnumerator();
        }
    }
}

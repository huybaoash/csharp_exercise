﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IEnumerable_Ex
{
    public class Module
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public int EmployeeId { get; set; }
        
        public DateTime Date { get; set; }
     
        public DateTime? CompletedDate { get; set; }

        private string _status;
        public string Status
        {
            get { return _status; }
            set
            {
                if (value != "Active" || value != "Pending" || value != "Inactive") return;
                else _status = value;
            }
        }

        public Module(Guid _id, string _name, int EmloyeeId, DateTime _date, DateTime? _completedate)
        {
            Id = _id;
            Name = _name;
            EmployeeId = EmloyeeId;
            Date = _date;
            CompletedDate = _completedate;

            // Nếu ngày hoàn thành ko có tức là Module thuộc trạng thái đang chờ xử lý --> Pending
            // Nếu ngày bắt đầu Module <= Ngày hiện tại <= Ngày kết thúc module ---> Active
            // Nếu ngày hiện tại < ngày bắt đầu module ---> Inactive 
            if (!CompletedDate.HasValue) _status = "Pending";
            else if ((DateTime.Now >= Date) && DateTime.Now <= CompletedDate.Value) _status = "Active";
            else if (DateTime.Now < Date) _status = "Inactive";
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.Practice.T02
{
    public class Program
    {
        public static long GetMaxSum(int[] inputArray, int subLength)
        {
            
            // Chuẩn bị một biến chứa danh sách những tổng số
            List<long> listsum = new List<long>();

            // Vòng lặp từ vị trí 0 đến vị trí độ dài mảng - 1;
            for (int i = 0; i < inputArray.Length; i++)
            {
                // Hướng đi của em như sau:
                // VD: cho mảng inputArray = { 1, 2, 5, -4, 3 };  số biến tính tổng yêu cầu = 4
                // Thì tổng lấy từ số vị trí inputArray[i] + inputArray[i+1] + inputArray[i+2] + inputArray[i+3]

                // Ta nhận thấy, nếu ở vị trí số 5 - Tức là inputArray[2]. Nếu dựa trên cách tính ở trên thì sẽ
                // lòi ra inputArray[5] => lỗi vì inputArray[4] là tối đa của mảng này.
                // Vì vậy điều kiện bắt buộc phải dừng là nếu tại vị trí i + độ dài số biến yêu cầu = độ dài mảng thì ngưng 
                // vòng lặp vì lúc này nó sẽ không gọi inputArray[5] nữa. 
                if (i + subLength - 1 == inputArray.Length) break;
                
                // Tạo biến tổng để chứa tổng những vị trí i,i+1,i+2,i+3,i+4,..v....v..... Tùy yêu cầu người truyền vào.
                long sum = 0;

                
                for (int j = i; j < i+subLength; j++)
                {
                    sum += inputArray[j];
                }

                // Sau khi tính tổng xong xuôi hết thì thêm vào mảng chứa những tổng.
                listsum.Add(sum);
                // Sau khi tính tổng tại vị trí i này sau thì sẽ tiếp tục tính tổng tại vị trí tiếp
                // theo rồi thêm vào list tổng.
            }
            
            // Trong listsum lúc này sẽ có các giá trị tổng. Ta chỉ việc trả về giá trị lớn nhất.
            return listsum.Max();
        }
        static void Main(string[] args)
        {
            int[] inputArray = { 1, 2, 5, -4, 3 }; 
            Console.WriteLine(GetMaxSum(inputArray,4));
            Console.ReadLine();
        }
    }
}

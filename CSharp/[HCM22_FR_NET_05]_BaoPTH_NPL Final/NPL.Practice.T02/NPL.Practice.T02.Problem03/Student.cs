﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.Practice.T02.Problem03
{
    //  Tạo class Student với những thuộc tính đã được đóng gói và có thể truy cập chúng thông qua những
    //  properties của class.
    //  Ta implement IGraduate theo yêu cầu đề bài.
    public class Student : IGraduate
    {
        // Đóng gói thuộc tính ID
        private int _id;
        // Truy suất hoặc gán giá trị thuộc tính ID thông qua biến này.
        public int ID
        {
            get { return _id; }
            set { _id = value; }
        }

        private string _name;

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        private DateTime _startDate;

        public DateTime StartDate
        {
            get { return _startDate; }
            set { _startDate = value; }
        }

        private decimal _sqlMark;

        public decimal SqlMark
        {
            get { return _sqlMark; }
            set { _sqlMark = value; }
        }

        private decimal _csharpMark;

        public decimal CsharpMark
        {
            get { return _csharpMark; }
            set { _csharpMark = value; }
        }

        private decimal _dsaMark;

        public decimal DsaMark
        {
            get { return _dsaMark; }
            set { _dsaMark = value; }
        }

        private decimal _gPA;

        public decimal GPA
        {
            get { return _gPA; }
            set { _gPA = value; }
        }

        // 1
        public enum GraduateLevel { 
            Excellent,
            VeryGood, 
            Good, 
            Average, 
            Failed
        };

        // Em tạo thuộc tính này để lưu trữ giá trị của GraduateLevel sau khi
        // phân loại GPA bởi method Graduate().

        private string _mygraduate_Level;

        public string MyGraduate_Level
        {
            get { return _mygraduate_Level; }
            set { _mygraduate_Level = value; }
        }

        // Tạo method Graduate theo yêu cầu đề bài.
        public void Graduate()
        {
            // Gán giá trị GPA = tổng điểm 3 môn chia cho 3.
            GPA =  (SqlMark + CsharpMark + DsaMark) / 3;
            // Để tiện cho việc so sánh với số thực ở dưới, em gọi tạm 1 biến chứa giá trị GPA
            // rồi ép kiểu nó thành float.
            float GPA_Cal = (float) GPA;
            // Nếu điểm trung bình GPA >= 9 thì thuộc tính MyGraduate_Level mà em tạo ở trên
            // sẽ lưu trữ lại giá trị của lớp enum này. Theo y/c đề bài thì enum lúc này = Excellent.
            if ( GPA_Cal >= 9.0)
            {
                MyGraduate_Level = GraduateLevel.Excellent.ToString();
                
            }
            else if (GPA_Cal >= 8.0 && GPA_Cal < 9.0)
            {
                MyGraduate_Level = GraduateLevel.VeryGood.ToString();
            }
            else if (GPA_Cal >= 7.0 && GPA_Cal < 8.0)
            {
                MyGraduate_Level = GraduateLevel.Good.ToString();
            }
            else if (GPA_Cal >= 5.0 && GPA_Cal < 7.0)
            {
                MyGraduate_Level = GraduateLevel.Average.ToString();
            }
            else if (GPA_Cal < 5.0)
            {
                MyGraduate_Level = GraduateLevel.Failed.ToString();
            }
        }

        public string GetCertificate()
        {
            return $"Name: {Name}, SqlMark: {SqlMark}, CsharpMark: {CsharpMark}, DsaMark: {DsaMark}" +
                $", GPA: {string.Format("{0:0.00}", GPA)}, GraduateLevel: {MyGraduate_Level}";
                // Định dạng lại cho kiểu decimal này ở dạng thập phân 2 đơn vị.
        }
    }
}

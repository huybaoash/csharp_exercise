﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.Practice.T02.Problem03
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Student student = new Student() { ID = 1,Name ="Peter", StartDate = DateTime.Now, 
            SqlMark = 10, CsharpMark = (decimal) 9.5, DsaMark = (decimal) 10
            };

            student.Graduate();
            Console.WriteLine(student.GetCertificate());
            
            Console.ReadLine();
        }
    }
}

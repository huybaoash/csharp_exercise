﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.Practice.T02.Problem01
{
    public class Program
    {
        public static string GetArticleSummary(string content, int maxLength)
        {
            // Tạo 1 biến lưu trữ chuỗi mà mình chuẩn bị tóm lược
            string articlesummary = "";
            // Ta phải so sánh độ dài chuỗi và độ dài tối đa theo yêu cầu.
            // Nếu độ dài chuỗi > độ dài tối đa theo yêu cầu thì thực thi tiếp.
            // Còn không thì chuỗi này không cần thiết phải tóm lược lại => trả về chuỗi nguyên mẫu.
            if (content.Length > maxLength)
            {
                // Chuỗi tóm lược chứa những ký tự từ vị trí 0 đến vị trí tối đa theo yêu cầu.
                articlesummary = content.Substring(0, maxLength);
                // Thêm dấu "..." rồi trả về 
                return articlesummary + "...";
            }
            return content;
        }
        static void Main(string[] args)
        {
            string sentence_ex = "One of the world's biggest festivals hit the streets of London";
            Console.WriteLine(GetArticleSummary(sentence_ex, 44));
         
            Console.ReadLine();
        }
    }
}

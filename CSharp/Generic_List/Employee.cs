﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generic_List
{
    public class Employee
    {
        public Employee(int _id, string _name, DateTime _dateJoin)
        {
            Id = _id;
            Name = _name;
            DateJoin = _dateJoin;
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public DateTime DateJoin { get; set; }


    }
}

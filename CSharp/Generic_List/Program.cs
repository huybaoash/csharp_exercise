﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.ComponentModel;

namespace Generic_List
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //1
            List2<string> s = new List2<string>()
            {
                "aaa","bb","ccc"
            };
            //1a
            s.Add("a");
            s.Remove("bb");
            //1b
            Console.WriteLine($"Count:{s.Count} members.");
            
            s.ToListLine();

            //2
            List3<Employee> e = new List3<Employee>()
            {
                new Employee (1,"aaa",DateTime.Now ),
                new Employee (2,"bbb",DateTime.Now.AddDays(1) ),
                new Employee (3,"ccc",DateTime.Now.AddMonths(1) )

            };
            //2a
            e.ToListAllPro_Type();

            Console.ReadLine();
        }

        

        

        
    }
}

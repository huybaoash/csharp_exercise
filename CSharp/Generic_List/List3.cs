﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generic_List
{
    public class List3<T> : List<T> where T : class
    {

        public void ToListAllPro_Type()
        {
            var list = this.ToList();
            foreach (var p in list)
            {
                // Search trên stackoverflow: TypeDescriptor thuộc namespace tên System.ComponentModel và là API mà Visual Studio sử dụng để hiển thị obj của mình trong trình duyệt thuộc tính (properties) của nó.
                // Trong mỗi thuộc tính thuộc danh sách thuộc tính của đối tượng p.
                foreach (PropertyDescriptor descriptor in TypeDescriptor.GetProperties(p))
                {
                    // In ra TOÀN BỘ: Tên thuộc tính và giá trị của thuộc tính đó của đối tượng.
                    string name = descriptor.Name;
                    object value = descriptor.GetValue(p);
                    Console.WriteLine("{0}:{1}", name, value);
                }
                Console.WriteLine($"Type:{p.GetType()}\n");

            }
        }
    }
}

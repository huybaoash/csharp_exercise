﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Polymophism_Overriding_Overloading
{
    public class Retangle : Shape
    {
        public override long Area()
        {
            return Width * Height;
        }

        public void Size(int sameSize)
        {

        }

        public void Size(int _width, int _height)
        {
            Width = _width;
            Height = _height;
            Console.WriteLine($"Width: {_width} \tHeight: {_height}");
        }

        public override string ToString()
        {
            return $"Width: {Width} \tHeight: {Height}";
        }

        public static Retangle operator +(Retangle retangle1,Retangle retangle2){
            Retangle sum = new Retangle()
            {
                Height = retangle1.Height + retangle2.Height,
                Width = retangle1.Width + retangle2.Width
            };
            Console.WriteLine($"RetangleSum({sum.Width},{sum.Height})");

            return sum;
        }

        public static Retangle operator -(Retangle retangle1, Retangle retangle2)
        {
            Retangle sub = new Retangle()
            {
                Height = retangle1.Height - retangle2.Height,
                Width = retangle1.Width - retangle2.Width
            };
            Console.WriteLine($"RetangleSub({sub.Width},{sub.Height})");

            return sub;
        }

    }
}

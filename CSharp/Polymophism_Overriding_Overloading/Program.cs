﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Polymophism_Overriding_Overloading
{
    public class Program
    {
        static void Main(string[] args)
        {
            //2
            Retangle retangle = new Retangle() { Width = 10,Height = 10 };
            Console.WriteLine(retangle.ToString());
            Console.WriteLine("Sau thay doi size cua Retangle:");
            retangle.Size(20, 20);

            //3
            Retangle retangle1  = new Retangle() { Width = 10, Height = 10 };
            Retangle retangle2 = new Retangle() { Width = 5, Height = 5 };

            Console.WriteLine("\n\n\nThuc hien toan tu giua 2 Retangle");
            Console.WriteLine(retangle1.ToString());
            Console.WriteLine(retangle2.ToString());


            Console.WriteLine("\n\nCong 2 Retangle");
            Retangle retanglesum = retangle1 + retangle2;

            Console.WriteLine("\n\nTru 2 Retangle");
            Retangle retanglesub = retangle1 - retangle2;

            Console.ReadLine();
        }
    }
}

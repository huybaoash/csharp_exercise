﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Polymophism_Overriding_Overloading
{
    public class Shape
    {
        public int Width { get; set; }
        public int Height { get; set; }

        public virtual long Area()
        {
            return (long)Width * (long)Height;
        } 

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace List_Dictionary
{
    public class Students
    {
        public int StudentID { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }
        public string Class_School { get; set; }



        public static void Add_5_Student(ref List<Students> students)
        {
            students.Add(new Students { StudentID = 2, Name = "AAA", Age = 18, Class_School = "12A1" });
            students.Add(new Students { StudentID = 3, Name = "BBB", Age = 17, Class_School = "11A2" });
            students.Add(new Students { StudentID = 1, Name = "CCC", Age = 18, Class_School = "11A4" });
            students.Add(new Students { StudentID = 4, Name = "DDD", Age = 17, Class_School = "11A1" });
            students.Add(new Students { StudentID = 5, Name = "EEE", Age = 16, Class_School = "10A1" });
        }

        public static bool Check_Contain(List<Students> students, Students student_check)
        {
            if (students.Contains(student_check)) return true;
            return false;
        }

        public static bool Check_Contain_ByID(List<Students> students, int ID)
        {
            foreach (Students student in students) 
                if (student.StudentID == ID) return true;
            return false;
        }

        public static void ToList_Desc(List<Students> students)
        {
            Compare_Student compare_Student = new Compare_Student();
            students.Sort(compare_Student);

            students.Reverse();



            foreach (var item in students)
            {
                Console.WriteLine($"\nStudent ID: {item.StudentID}\tName: {item.Name}");
                Console.WriteLine($"Age: {item.Age}\tClass: {item.Class_School}\n");

            };
        }

        public static void ToList(List<Students> students)
        {
            foreach (var item in students)
            {
                Console.WriteLine($"\nStudent ID: {item.StudentID}\tName: {item.Name}");
                Console.WriteLine($"Age: {item.Age}\tClass: {item.Class_School}\n");

            };
        }



        public class Compare_Student : IComparer<Students>
        {
            public int Compare(Students x, Students y)
            {
                return x.StudentID.CompareTo(y.StudentID);
            }
        }
    }
}

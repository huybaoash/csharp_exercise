﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace List_Dictionary
{
    internal class Program
    {
        // 1
        
        static void Main(string[] args)
        {
            //2
            List<Students> students = new List<Students>();
            Students.Add_5_Student(ref students);
            Students.ToList(students);
            //3
            if (Students.Check_Contain_ByID(students,5)) 
                Console.WriteLine("Co ton tai !");
            else 
                Console.WriteLine("Khong ton tai !");

            //4 
            Console.WriteLine("\n\n\nSau khi sap xep giam dan !");
            Students.ToList_Desc(students);

            Console.ReadLine();
        }
    }
}

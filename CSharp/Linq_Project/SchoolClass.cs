﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Linq_Project
{
    public class SchoolClass
    {
        public int IDclass { get; set; }
        public string Name { get; set; }

        public static List<SchoolClass> initList(ref List<SchoolClass> list)
        {
            list.Add(new SchoolClass() { IDclass = 1, Name = "12A1"});
            list.Add(new SchoolClass() { IDclass = 2, Name = "12A2" });
            list.Add(new SchoolClass() { IDclass = 3, Name = "12A3" });

            return list;
        }
    }
}

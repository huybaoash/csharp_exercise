﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Linq_Project
{
    public class Student
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public int IDclass { get; set; }

        public static List<Student> initList(ref List<Student> list)
        {
            list.Add(new Student() { IDclass = 1, Name = "HAHAHA",Age = 18 });
            list.Add(new Student() { IDclass = 2, Name = "HIHIHI", Age = 19 });
            list.Add(new Student() { IDclass = 3, Name = "HEHEHE",Age =18 });
            list.Add(new Student() { IDclass = 1, Name = "HOHOHO", Age = 18 });
            list.Add(new Student() { IDclass = 2, Name = "HUHUHU", Age = 18 });
            list.Add(new Student() { IDclass = 3, Name = "NONONO", Age = 18 });
            list.Add(new Student() { IDclass = 1, Name = "YESYESYES", Age = 18 });
            list.Add(new Student() { IDclass = 2, Name = "TRUETRUETRUE", Age = 18 });
            list.Add(new Student() { IDclass = 3, Name = "FALSEFALSEFALSE", Age = 18 });
            return list;
        }

        public override string ToString()
        {
            return $"\nName: {Name}\t Age:{Age}\nClass ID: {IDclass}";
        }

        public static IEnumerable AllStudent(List<Student> students,List<SchoolClass> classes) {
            var std_enum = students.AsEnumerable();
            var clss_enum = classes.AsEnumerable();
           
            var students_byclass =
                                    from student in std_enum 
                                    join clss in clss_enum 
                                    on student.IDclass equals clss.IDclass
                                    
                                    select new { ClassID = student.IDclass, Name = student.Name, ClassName = clss.Name };
            


            return students_byclass.OrderBy(s => s.ClassID).ToList();
        }

        public static IEnumerable AllStudent_ByClassID(List<Student> students, int IDclass)
        {
            var std_enum = students.AsEnumerable();
            

            var students_byclass = std_enum.Where(id => id.IDclass == IDclass);



            return students_byclass.ToList();
        }

        public static void  ToList(IEnumerable list)
        {
            Console.WriteLine("\n\nToan bo so hoc sinh");
            foreach (var item in list)
            {
                Console.WriteLine(item.ToString());
            }
        }

        public static int CountStudent(IEnumerable list)
        {
            int count = 0;
            foreach (var item in list)
            {
                count++;
            }
            return count;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Linq_Project
{
    internal class Program
    {
        static void Main(string[] args)
        {
            List<Student> students = new List<Student>(); Student.initList(ref students);
            List<SchoolClass> sclass = new List<SchoolClass>(); SchoolClass.initList(ref sclass);

            //GroupJoin theo lop co ID=1
            Console.WriteLine("Class ID=1");
            var students_byclassID = Student.AllStudent_ByClassID(students,1);
            Student.ToList(students_byclassID);

            // So luong HS cua lop co ID=1
            int total_student = Student.CountStudent(students_byclassID);
            Console.WriteLine($"Total Student: {total_student}");

            //Toan bo hoc sinh voi day du thong tin class (GROUP JOIN) va sap xep thu tu theo lop
            var students_group = Student.AllStudent(students, sclass);
            Student.ToList(students_group);
            Console.ReadLine();
        }
    }
}

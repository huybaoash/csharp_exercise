﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inheritance_Interface_Abstract
{
    public class PizzaOrder : PaymentInformation, IOrder
    {
        private int _price = 50000,_amount = 0;
        private bool _isOrder = false;

        public PizzaOrder(): base()
        {
           

        }

        public PizzaOrder(int amount)
        {
            _amount = amount;
        }
        public int Amount {
            get { return _amount; }
            set { _amount = value; }
        }

        public override Guid GetPaymentNo()
        {
            return base.GetPaymentNo();
        }

        public void CancelOrder()
        {
            if (_isOrder == true)
            {
                Console.WriteLine("Huy thanh cong !");
                _isOrder = false;
            }
            else Console.WriteLine("Chua dat hang !");

        }

        public void GetOrder()
        {

            Console.WriteLine($"Payment No: {GetPaymentNo()}");
            Console.WriteLine($"TOTAL PRICE: {_price * _amount}");
            Console.WriteLine($"STATUS: {_isOrder}\n");
        }



        public void PlaceOrder()
        {
            if (_amount == 0)
            {
                Console.WriteLine("Moi nhap Amount:");
                int a = Convert.ToInt32(Console.ReadLine());
                _amount = a;
                
            }
            else
            {
                if (_isOrder == true) Console.WriteLine("Ban da dat roi !\n");
                else {
                    Console.WriteLine("Dat order thanh cong !\n");
                    _isOrder = true;
                    GetOrder();
                    
                }
            }
        }
    }
}

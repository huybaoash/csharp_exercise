﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inheritance_Interface_Abstract
{
    public abstract class PaymentInformation
    {
        private Guid _paymentNo;

        public virtual Guid GetPaymentNo()
        {
            return _paymentNo;
        }

        public PaymentInformation()
        {
            _paymentNo = Guid.NewGuid();
        }

        public PaymentInformation(Guid paymentNo)
        {
            _paymentNo = paymentNo;
        }

    }
}

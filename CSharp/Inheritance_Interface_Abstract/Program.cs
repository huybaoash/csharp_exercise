﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inheritance_Interface_Abstract
{
    public class Program
    {
        static void Main(string[] args)
        {
            PizzaOrder order = new PizzaOrder();
            order.PlaceOrder(); // Nhap Amount cho order
            order.PlaceOrder(); // Vi Is_Order = false nen in ra thông tin + set Is_Order = true
            order.PlaceOrder(); // Vi Is_Order luc nay = true nen in ra "Ban da dat roi !"
            order.CancelOrder(); // Huy Order thanh cong
         

            order.GetOrder();
            Console.ReadLine();
        }
    }
}
